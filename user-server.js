/*
	Wir verwenden nodemon - dieses wird als script in package.json
	angegeben: "start". mit npm run server können wir nun das angegebene
	script laufen lassen. Mit jeder Änderung starte node das script
	automatisch neu.

	Wir testen die restful api mit der thunderclient extension in
	VSCODE. Postman bzw. auch andere rest clients sind möglich.
*/
import express from "express"
import userRouter from "./routes-user.js"

const app = express()

/**
 * Home Route
 */
app.get("/", (req, res) => {
	res.send("Welcome Home Response")
})

app.use("/user", userRouter)

app.listen(3000, () => console.log("Listening at http://localhost:3000"))
