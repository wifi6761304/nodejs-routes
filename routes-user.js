import express from "express"
import bodyParser from "body-parser"
/*
	Mit Hilfe des routers können wir beliebige routes in einem Modul
	definieren. In unserer user-server.js weisen wir dann eine
	übergeordneten url alle unsere routes zu:
	app.use("/user", router)

	Von nun an können unsere hier definierten Routes über /user aufgerufen werden.
*/
const userRouter = express.Router()
userRouter.use(bodyParser.urlencoded({ extended: true }))

userRouter.get("/", (req, res) => {
	res.send("Overview of all Users in new server")
})

userRouter.get("/:userid", (req, res) => {
	const userId = req.params.userid
	res.send(`Show user with id ${userId}`)
})

// TODO: create all other restful routes for user

export default userRouter
