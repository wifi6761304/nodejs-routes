/*
	Wir verwenden nodemon - dieses wird als script in package.json
	angegeben: "start". mit npm run server können wir nun das angegebene
	script laufen lassen. Mit jeder Änderung starte node das script
	automatisch neu.

	Wir testen die restful api mit der thunderclient extension in
	VSCODE. Postman bzw. auch andere rest clients sind möglich.
*/
import express from "express"
import bodyParser from "body-parser"

const app = express()
app.use(bodyParser.urlencoded({ extended: true }))

/**
 * Home Route
 */
app.get("/", (req, res) => {
	res.send("Welcome Home Response")
})

/**
 * Users overview
 */
app.get("/user", (req, res) => {
	res.send("All users shown here")
})

/**
 * Show single user
 */
app.get("/user/:userid", (req, res) => {
	/*
		https://expressjs.com/en/guide/routing.html
		Routes parameters
		Teile der route können variabel sein. Dh. an der Stelle, an der ich z. B. eine
		Id erwarte (userid), setze ich in der URL einen Platzhalter :paramname ein.
		Diese Platzhalter können dann über req.params.paramname ausgelesen werden.
		Es können beliebig viele Parameter in einer url gesetzt werden.
		ACHTUNG: Jede URL + HTTP Verb ist einzigartig. /user/:userid und user/:userid/:page
		müssen mit zwei verschiedenen routes abgefangen werden.
	*/
	const userid = req.params.userid
	// TODO: prüfen, ob userid eine integer ist.
	res.send(`Data for user with id ${userid}`)
})

/**
 * Add new user
 */
app.post("/user", (req, res) => {
	/*
		req.body enthält die Daten, die gepostet wurden
		ACHTUNG: nicht vergessen, body-parser middleware muss aktiviert
		ACHTUNG: beim Testen mit z.B. thunderclient oder postman
		Header setzen:
		default Formular header sind application/x-www-form-urlencoded,
		beim upload von Dateien muss der header auf multipart/form-data
		gesetzt werden.
	*/
	const userName = req.body.userName
	const email = req.body.email
	res.send(`Added new User: ${userName}, ${email}`)
})

/**
 * Delete a single user
 */
app.delete("/user/:userid", (req, res) => {
	const userid = req.params.userid
	res.send(`User ${userid} deleted`)
})

/**
 * Update/patch a single user
 */
app.patch("/user/:userid", (req, res) => {
	const userId = req.params.userid
	const userName = req.body.username
	const email = req.body.email
	res.send(`Added new User: ${userName}, ${email} with id ${userId}`)
})

app.listen(3000, () => console.log("Listening at http://localhost:3000"))
